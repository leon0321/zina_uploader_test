#/usr/bin/python
# -*- coding: utf-8 -*-
from piston3.handler import BaseHandler
from django.contrib.auth.models import User
from piston3.utils import rc, throttle
from zina_uploader_test.forms import UserForm

# custom status for piston
rc.CODES.update(SUCCESS=('SUCCESS', 201))  # 201 Created
rc.CODES.update(ERROR=('ERROR', 203))  # 203 Non-Authoritative Information


class UserHandler(BaseHandler):
    allowed_methods = ('GET', 'POST', 'PUT')
    model = User
    fields = ('zipfile', 'creation_date', 'updated_date', 'number', 'serie', 'portal_protocol')

    def read(self, request, user_id):
        print 'user_id: ', user_id
        try:
            user = User.objects.get(id=user_id)
        except User.DoesNotExist, e:
            resp = rc.ERROR
            resp.write(': ' + e.message)
            return resp
        return user

    @throttle(60, 60)
    def create(self, request):
        print 'POST: ', request.POST
        user_id = request.POST.get('id', None)
        if user_id:  # update item
            try:
                user = User.objects.get(id=user_id)
            except User.DoesNotExist, e:
                resp = rc.ERROR
                resp.write(': ' + e.message)
                return resp
            form = UserForm(request.POST, request.FILES, instance=user)
        else:
            form = UserForm(request.POST, request.FILES)
        print form
        if form.is_valid():
            try:
                form.save()
            except Exception, e:
                resp = rc.ERROR
                resp.write(': ' + repr(e))
                return resp

            return rc.SUCCESS

        resp = rc.ERROR
        resp.write(': ' + repr(form.errors))
        return resp