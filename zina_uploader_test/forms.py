from django import forms
from django.contrib.auth.models import User


class UserForm(forms.ModelForm):
    zipfile = forms.FileField(required=True)

    class Meta:
        model = User
        fields = ('zipfile',)
